/* Copyright 2017, Gigoló Pura Sangre
 * All rights reserved.
 *
 * This file is part of Workspace.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/

#include <stdio.h>
#include "app_multicore_cfg.h"
#include "ipc_example.h"
#include "ipc_msg.h"

/*==================[macros and definitions]=================================*/
/*** SE DEBE CONFIGURAR DE LA MISMA MANERA EN EL CÓDIGO DEL CORTEX-M0 ***
 *
 * Start up -> START_UP en 1, todo lo otro en 0
 * Interrupt RTT ->  INTERRUPT_SIGNAL en 1, RTT en 1, todo lo otro en 0
 * Memory Access RTT -> MEMORY_ACCESS en 1, RTT en 1, todo lo otro en 0
 * Interrupt ->  INTERRUPT_SIGNAL en 1, todo lo otro en 0
 * Memory Access -> MEMORY_ACCESS en 1, todo lo otro en 0
 * Caso real -> INTERRUPT_SIGNAL en 1, RTT en 1, LPCOPEN_FUNCTIONS en 1, REAL_COMMUNICATION en 1, todo lo otro en 0
 */

/* Tests */
#define START_UP 0
#define MEMORY_ACCESS 0
#define INTERRUPT_SIGNAL 1
#define RTT 1
#define LPCOPEN_FUNCTIONS 0

#if LPCOPEN_FUNCTIONS // Si no se usa LPCOPEN no se puede hacer una comunicación real
#define REAL_COMMUNICATION 0
#endif

#define TRUE 1
#define FALSE 0

/* Macros para la comunicación */
#define MESSAGE 0xFFFFFFFF
#define MESSAGE_M4_TO_M0 0x123
#define MESSAGE_M0_TO_M4 0x1234
#define EXPECTED_M0_RESPONSE 0xAAAAAAAA

#define DELAY_MS 500

#define CIAA_MULTICORE_IPC_MAILBOX1_ADDR (*((volatile unsigned long *) 0x20008000))
#define CIAA_MULTICORE_IPC_MAILBOX2_ADDR (*((volatile unsigned long *) 0x20008004))
#define CIAA_MULTICORE_CORE_1_IMAGE ((uint8_t *)0x1B000000)

#define SEG_TO_USEG(t) (t *= 1000000)

/*==================[internal data declaration]==============================*/

volatile uint32_t * DWT_CTRL = (uint32_t *)0xE0001000;
volatile uint32_t * DWT_CYCCNT = (uint32_t *)0xE0001004;

/** @brief used for delay counter */
static uint32_t pausems_count;
static float tiempo;
/*==================[internal functions declaration]=========================*/

/** @brief delay function
 * @param t desired milliseconds to wait
 */
static void pausems(uint32_t t);
static void prvSetupHardware(void);
/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/* initialization routine for Multicore examples */
static void prvSetupHardware(void)
{
	SystemCoreClockUpdate();
	Board_Init();

	/* Resetear zonas de memoria compartida */
	CIAA_MULTICORE_IPC_MAILBOX1_ADDR = 0;
	CIAA_MULTICORE_IPC_MAILBOX2_ADDR = 0;

	#if !START_UP
	/* Time to Start M0APP */
	if (M0Image_Boot(CPUID_M0APP, (uint32_t) BASE_ADDRESS_M0APP) < 0) {
		DEBUGSTR("Unable to BOOT M0APP Core!");
	}
	#endif

	#if LPCOPEN_FUNCTIONS
	/* Initialize the IPC Queue */
	IPCEX_Init();
	#endif

	#if INTERRUPT_SIGNAL
	NVIC_EnableIRQ(M0APP_IRQn);
	#endif

	/* Start the tick timer */
	SysTick_Config(SystemCoreClock / 1000);

}

static void pausems(uint32_t t)
{
	pausems_count = t;
	while (pausems_count != 0) {
		__WFI();
	}
}

/*==================[external functions definition]==========================*/

void SysTick_Handler(void)
{
	if(pausems_count > 0) pausems_count--;
}

#if RTT
void M0APP_IRQHandler (void)
{
	#if REAL_COMMUNICATION
	ipcex_msg_t msg;
	uint32_t message;
	Chip_CREG_ClearM0AppEvent(); // Limpiar el flag de la interrupción
	if (IPC_tryPopMsg(&msg) != QUEUE_VALID) {
		return;
	}
	switch (msg.id.pid) {
	case MESSAGE_M0_TO_M4:
		message = msg.data1;
		if (message == EXPECTED_M0_RESPONSE)
		{
			//TODO: Procesar dato
		}
		else
		{
			DEBUGOUT("Error en el envío del mensaje");
		}
		break;
	default:
		DEBUGOUT("El mensaje no es para el M4");
	}
	#endif

	tiempo = (float)(*DWT_CYCCNT); // Obtener los ticks

	#if !REAL_COMMUNICATION
	Chip_CREG_ClearM0AppEvent(); // Limpiar el flag de la interrupción
	#endif

	SEG_TO_USEG(tiempo); // Convertir a useg
	tiempo /= (float)SystemCoreClock;	// Obtener el tiemp
	__ASM("nop"); // Punto para poner el break point
}
#endif

int main(void)
{
	prvSetupHardware();
	ipcex_msg_t msg;
	#if REAL_COMMUNICATION
	/* Inicializar los campos de la cola */
	msg.id.pid = MESSAGE_M4_TO_M0;
	msg.id.cpu = CPUID_M0APP;
	#endif
	while (TRUE) {
		#if START_UP
		CIAA_MULTICORE_IPC_MAILBOX1_ADDR = 0;
		*DWT_CTRL |= 1; // Habilitar el contador de ciclos
		*DWT_CYCCNT = 0; // Resetar el contador de ciclos
		M0Image_Boot(CPUID_M0APP, (uint32_t) BASE_ADDRESS_M0APP);
		while(CIAA_MULTICORE_IPC_MAILBOX1_ADDR == 0);
		tiempo = (float)(*DWT_CYCCNT); // Obtener los ticks
		SEG_TO_USEG(tiempo); // Convertir a useg
		tiempo /= (float)SystemCoreClock;	// Obtener el tiempo
		__ASM("nop"); // Punto para poner el break point
		break; // Termina el while, el start up se mide una sola vez
		#endif
		#if INTERRUPT_SIGNAL
		/* Cada 500ms mandar una señal de interrupción */
		pausems(DELAY_MS);
		*DWT_CTRL |= 1;	// Habilitar el contador de ciclos
		*DWT_CYCCNT = 0;  // Resetar el contador de ciclos
		/* Generar pulso de interrupción */
		#if LPCOPEN_FUNCTIONS
		#if REAL_COMMUNICATION
		msg.data0 = MESSAGE;
		#endif
		IPC_tryPushMsg(msg.id.cpu, &msg);
		#else
			__DSB();
			__SEV();
		#endif
		#if !RTT
		while(CIAA_MULTICORE_IPC_MAILBOX1_ADDR == 0); // Esperar la respuesta
		tiempo = (float)(*DWT_CYCCNT); // Obtener los ticks
		SEG_TO_USEG(tiempo); // Convertir a useg
		tiempo /= (float)SystemCoreClock;	// Obtener el tiempo
		__ASM("nop"); // Punto para poner el break point
		CIAA_MULTICORE_IPC_MAILBOX1_ADDR = 0; // Limpiar el sector de memoria con el dato
		#endif
		#endif
		#if MEMORY_ACCESS
		unsigned long lectura;
		*DWT_CTRL |= 1;	// Habilitar el contador de ciclos
		*DWT_CYCCNT = 0;  // Resetar el contador de ciclos
		CIAA_MULTICORE_IPC_MAILBOX2_ADDR = 1; // Mandar un dato
		while(CIAA_MULTICORE_IPC_MAILBOX1_ADDR == 0); // Esperar la respuesta
		#if RTT
		lectura = CIAA_MULTICORE_IPC_MAILBOX1_ADDR; // Recuperar dato
		CIAA_MULTICORE_IPC_MAILBOX1_ADDR = 0; // Limpiar el sector de memoria con el dato
		#endif
		tiempo = (float)(*DWT_CYCCNT); // Obtener los ticks
		SEG_TO_USEG(tiempo); // Convertir a useg
		tiempo /= (float)SystemCoreClock;	// Obtener el tiempo
		#if !RTT
		lectura = CIAA_MULTICORE_IPC_MAILBOX1_ADDR; // Recuperar dato
		CIAA_MULTICORE_IPC_MAILBOX1_ADDR = 0; // Limpiar el sector de memoria con el dato
		#endif
		__ASM("nop"); // Punto para poner el break point
		#else
		__WFI();
		#endif
	}
	return 0;
}

/*==================[end of file]============================================*/
