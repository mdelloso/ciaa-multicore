/* Copyright 2017, Gigoló Pura Sangre
 * All rights reserved.
 *
 * This file is part of Workspace.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/

#include <stdio.h>
#include "app_multicore_cfg.h"
#include "ipc_example.h"
#include "ipc_msg.h"

/*==================[macros and definitions]=================================*/
/*** SE DEBE CONFIGURAR DE LA MISMA MANERA EN EL CÓDIGO DEL CORTEX-M4 ***
 *
 * Start up -> START_UP en 1, todo lo otro en 0
 * Interrupt RTT ->  INTERRUPT_SIGNAL en 1, RTT en 1, todo lo otro en 0
 * Memory Access RTT -> MEMORY_ACCESS en 1, RTT en 1, todo lo otro en 0
 * Interrupt ->  INTERRUPT_SIGNAL en 1, todo lo otro en 0
 * Memory Access -> MEMORY_ACCESS en 1, todo lo otro en 0
 * Caso real -> INTERRUPT_SIGNAL en 1, RTT en 1, LPCOPEN_FUNCTIONS en 1, REAL_COMMUNICATION en 1, todo lo otro en 0
 */

/* Tests */
#define START_UP 0
#define MEMORY_ACCESS 0
#define INTERRUPT_SIGNAL 1
#define RTT 1
#define LPCOPEN_FUNCTIONS 0

#if LPCOPEN_FUNCTIONS // Si no se usa LPCOPEN no se puede hacer una comunicación real
#define REAL_COMMUNICATION 0
#endif

#define TRUE 1
#define FALSE 0

/* Macros para la comunicación */
#define MESSAGE 0xAAAAAAAA
#define MESSAGE_M4_TO_M0 0x123
#define MESSAGE_M0_TO_M4 0x1234
#define EXPECTED_M4_MESSAGE 0xFFFFFFFF

#define CIAA_MULTICORE_IPC_MAILBOX1_ADDR (*((volatile unsigned long *) 0x20008000))
#define CIAA_MULTICORE_IPC_MAILBOX2_ADDR (*((volatile unsigned long *) 0x20008004))

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/
#if LPCOPEN_FUNCTIONS
ipcex_msg_t msg;
#endif
/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

static void prvSetupHardware(void)
{
	SystemCoreClockUpdate();

	#if LPCOPEN_FUNCTIONS
	/* Initialize the IPC Queue */
	IPCEX_Init();
	#endif

	#if INTERRUPT_SIGNAL
	NVIC_EnableIRQ(M4_IRQn);
	#endif
}

/*==================[external functions definition]==========================*/
#if INTERRUPT_SIGNAL
void M4_IRQHandler(void)
{
	#if RTT
	/* Generar pulso de interrupción */
	#if LPCOPEN_FUNCTIONS
	#if REAL_COMMUNICATION

	ipcex_msg_t msg;
	uint32_t message;
	Chip_CREG_ClearM0AppEvent(); // Limpiar el flag de la interrupción

	if (IPC_tryPopMsg(&msg) != QUEUE_VALID) {
		return;
	}
	switch (msg.id.pid) {
	case MESSAGE_M4_TO_M0:
		message = msg.data0;
		if (message == EXPECTED_M4_MESSAGE)
		{
			msg.id.pid = MESSAGE_M0_TO_M4;
			msg.id.cpu = CPUID_M4;
			msg.data1 = MESSAGE;
		}
		else
		{
			DEBUGOUT("Error en el envío del mensaje");
		}
		break;
	default:
		DEBUGOUT("El mensaje no es para el M0");
	}

	#endif
	IPC_tryPushMsg(msg.id.cpu, &msg);
	#else
	__DSB();
	__SEV();
	#endif
	#else
	CIAA_MULTICORE_IPC_MAILBOX1_ADDR = 1;
	#endif

	#if !REAL_COMMUNICATION
	Chip_CREG_ClearM0AppEvent(); // Limpiar el flag de la interrupción
	#endif
}
#endif
int main(void)
{
	#if START_UP
	CIAA_MULTICORE_IPC_MAILBOX1_ADDR = 1;
	#endif
	prvSetupHardware();
	while (TRUE) {
		#if MEMORY_ACCESS
		unsigned long lectura;
		while(CIAA_MULTICORE_IPC_MAILBOX2_ADDR == 0);
		#if !RTT
		CIAA_MULTICORE_IPC_MAILBOX1_ADDR = 1;
		#endif
		lectura = CIAA_MULTICORE_IPC_MAILBOX2_ADDR;
		CIAA_MULTICORE_IPC_MAILBOX2_ADDR = 0;
		#if RTT
		CIAA_MULTICORE_IPC_MAILBOX1_ADDR = 1;
		#endif
		#else
		__WFI();
		#endif
	}
	return 0;
}

/*==================[end of file]============================================*/
